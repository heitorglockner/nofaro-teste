/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')
import VueRouter from 'vue-router'

window.Vue.use(VueRouter)

Vue.config.productionTip = false

import PeopleIndex from './components/PeopleIndex'
import PeopleCreate from './components/PeopleCreate'

const routes = [
    {
        path: '/',
        component: PeopleIndex
    },
    {
        path: '/create',
        component: PeopleCreate,
        name: 'createPeople'
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

const app = new Vue({
    router
}).$mount('#app')
