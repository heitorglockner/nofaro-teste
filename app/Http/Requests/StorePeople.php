<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePeople extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'name' => 'required|min:2',
			'email' => 'required|unique:people|email',
			'ddd' => 'max:2',
			'phone' => 'nullable|digits:9'
		];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O campo nome é obrigatório.',
            'name.min' => 'O campo nome deve ter pelo menos 2 caracteres.',
            'email.required'  => 'O campo e-mail é obrigatório.',
            'email.unique'  => 'O e-mail digitado já existe.',
            'email.email'  => 'O e-mail digitado é inválido.',
            'ddd.max'  => 'O campo DDD deve conter no máximo dois dígitos.',
            'phone.digits'  => 'O campo telefone deve conter 9 dígitos.',
        ];
    }
}
