<?php

namespace App\Http\Controllers\Api;

use App\People;
use App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Requests\StorePeople;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $perPage = 25;
        $people = People::orderBy('name');

        if (!empty($name)) {
            $people->where('name', 'LIKE', "%$name%");
        }

        if (empty($name) && !empty($email)) {
            $people->where('email', $email);
        } elseif (!empty($email)) {
            $people->orWhere('email', $email);
        }

        $people = $people->paginate($perPage);

        return response()->json($people);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StorePeople $request)
    {
        $requestData = $request->all();

        People::create($requestData);

        return response()->json([
            'message' => 'Pessoa Adicionada!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        People::destroy($id);

        return response()->json([
            'message' => 'Pessoa Excluída!'
        ]);
    }
}
