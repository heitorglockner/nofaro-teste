<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'ddd', 'phone'];

    
}
