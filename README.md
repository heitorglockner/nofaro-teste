# Para executar a aplicação seguir os seguintes passos:
## composer install
## php artisan migrate
## npm install
## npm run prod
## copiar .env.example para .env

## Ajustar configurações no arquivo .env de acesso ao banco:
DB_CONNECTION=mysql \
DB_HOST=NOME_DO_SERVIDOR_DO_BANCO_DE_DADOS \
DB_PORT=3306 \
DB_DATABASE=NOME_DA_BASE_DE_DADOS \
DB_USERNAME=NOME_DO_USUÁRIO_DO_BANCO_DE_DADOS \
DB_PASSWORD=SENHA_DO_USUÁRIO_DO_BANCO_DE_DADOS